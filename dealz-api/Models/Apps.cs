using Newtonsoft.Json;
using System.Collections.Generic;

namespace dealz_api.Models
{
    public class Apps
    {
        [JsonProperty("apps")]
        public List<App> Appss { get; set; }
    }
}