using Newtonsoft.Json;

namespace dealz_api.Models
{
    public class AppList
    {
        [JsonProperty("appList")]
        public Apps Apps { get; set; }
    }
}