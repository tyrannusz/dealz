using dealz_api.Models;
using dealz_api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace dealz_api.Controllers
{
    [Route("api/SteamData")]
    public class SteamDataController : Controller
    {
        private readonly IDataService _steamDataService;

        public SteamDataController(IDataService steamDataService)
        {
            _steamDataService = steamDataService;
        }

        [HttpGet]
        public async Task<AppList> SteamData()
        {
            return await _steamDataService.GetData<AppList>().ConfigureAwait(false);
        }

        [HttpGet("{appId}")]
        public async Task<Dictionary<string, Game>> SteamData([FromRoute] int appId)
        {
            return await _steamDataService.GetData<Dictionary<string, Game>>(appId).ConfigureAwait(false);
        }
    }
}