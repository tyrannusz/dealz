using System.Threading.Tasks;

namespace dealz_api.Services
{
    public interface IDataService
    {
        Task<T> GetData<T>();
        Task<T> GetData<T>(int id);
    }
}