using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace dealz_api.Services
{
    public class SteamDataService : IDataService
    {
        private string key = "A81523C4D1060C253973E5178D0B3294";

        public async Task<T> GetData<T>()
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync($"https://api.steampowered.com/ISteamApps/GetAppList/v2/").ConfigureAwait(false);
                return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        public async Task<T> GetData<T>(int appId)
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync($"https://store.steampowered.com/api/appdetails?appids={appId}&key={key}").ConfigureAwait(false);
                return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }
    }
}