export interface Highlighted {
    name: string;
    path: string;
}

export interface Achievements {
    highlighted: Highlighted[];
    total: number;
}

export interface Category {
    description: string;
    id: number;
}

export interface ContentDescriptors {
    ids: any[];
    notes?: any;
}

export interface Demo {
    appid: number;
    description: string;
}

export interface Genre {
    description: string;
    id: string;
}

export interface MacRequirements {
    minimum: string;
    recommended: string;
}

export interface Metacritic {
    score: number;
    url: string;
}

export interface Webm {
    480: string;
    max: string;
}

export interface Movie {
    highlight: boolean;
    id: number;
    name: string;
    thumbnail: string;
    webm: Webm;
}

export interface Sub {
    can_get_free_license: string;
    is_free_license: boolean;
    option_description: string;
    option_text: string;
    packageid: number;
    percent_savings: number;
    percent_savings_text: string;
    price_in_cents_with_discount: number;
}

export interface PackageGroup {
    description: string;
    display_type: number;
    is_recurring_subscription: string;
    name: string;
    save_text: string;
    selection_text: string;
    subs: Sub[];
    title: string;
}

export interface PcRequirements {
    minimum: string;
    recommended: string;
}

export interface Platforms {
    linux: boolean;
    mac: boolean;
    windows: boolean;
}

export interface PriceOverview {
    currency: string;
    discount_percent: number;
    final: number;
    final_formatted: string;
    initial: number;
    initial_formatted: string;
}

export interface Recommendations {
    total: number;
}

export interface ReleaseDate {
    coming_soon: boolean;
    date: string;
}

export interface Screenshot {
    id: number;
    path_full: string;
    path_thumbnail: string;
}

export interface SupportInfo {
    email: string;
    url: string;
}

export interface Data {
    about_the_game: string;
    achievements: Achievements;
    background: string;
    categories: Category[];
    content_descriptors: ContentDescriptors;
    demos: Demo[];
    detailed_description: string;
    developers: string[];
    dlc: number[];
    genres: Genre[];
    header_image: string;
    is_free: boolean;
    metacritic: Metacritic;
    movies: Movie[];
    name: string;
    package_groups: PackageGroup[];
    packages: number[];
    platforms: Platforms;
    price_overview: PriceOverview;
    publishers: string[];
    recommendations: Recommendations;
    release_date: ReleaseDate;
    required_age: number;
    reviews: string;
    screenshots: Screenshot[];
    short_description: string;
    steam_appid: number;
    support_info: SupportInfo;
    supported_languages: string;
    type: string;
    website: string;
}

export interface Game {
    data: Data;
    success: boolean;
}

export interface GameRoot {
    [key: string] : Game
}

