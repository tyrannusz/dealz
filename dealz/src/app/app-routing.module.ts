import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SteamComponent } from './steam/steam.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'steam', component: SteamComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
