import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/app.httpservice';
import { AppList } from '../models/AppList';
import { App } from '../models/App';
import { GameRoot } from '../models/Game';
import { Game } from '../models/Game';

@Component({
  selector: 'app-steam',
  templateUrl: './steam.component.html',
  styleUrls: ['./steam.component.sass']
})
export class SteamComponent implements OnInit {
    title = 'Steam';
    steamAppList: AppList;
    gameInput: string;
    showError: boolean;
    errorText: string;
    showGameResult: boolean;
    gameBackgroundImage: string;
    gameResultName: string;
    discountedPrice: string;
    gamesFoundInSearch: Game[];
    constructor(private httpService: HttpService){}

    ngOnInit(){
        this.gameInput = "";
        this.showError = false;
        this.errorText = "";
        this.showGameResult = false;
        this.gameBackgroundImage = "";
        this.gameResultName = "";
        this.discountedPrice = "";
        this.gamesFoundInSearch = [];
        this.getSteamApps();
    }

    getSteamApps(){
        this.httpService.getSteamApps().subscribe((data: AppList) => this.steamAppList = data);
    }

    getGameInfo(){
        //find app
        var foundApp = this.steamAppList.appList.apps.filter(x => x.name.toUpperCase().trim().replace(/\s/g, "").includes(this.gameInput.toUpperCase().trim().replace(/\s/g, "")))

        //reset view controls
        this.showError = false;
        this.errorText = "";
        this.showGameResult = false;
        this.gameBackgroundImage = "";
        this.gameResultName = "";
        this.discountedPrice = "";
        this.gamesFoundInSearch = [];

        //if the app was not found
        if(foundApp == null)
        {
            this.showError = true;
            this.errorText = this.gameInput;
        }
        else
        {
            //grab game info
            foundApp.forEach(element => {
                this.httpService.getGameInfo(element.appId).subscribe((data: GameRoot) => 
                    {
                        console.log(data[element.appId].data.price_overview.final_formatted);
                        this.gamesFoundInSearch.push(data[element.appId]);
                    }
                );
            });

            this.showGameResult = true;
        }

        //reset input
        this.gameInput = "";
    }
}
