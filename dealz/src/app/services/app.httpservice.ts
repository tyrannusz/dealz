import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { AppList } from '../models/AppList';
import { GameRoot } from '../models/Game';

@Injectable()
export class HttpService {
    constructor(private httpClient: HttpClient){}

    getSteamApps(){
        return this.httpClient.get<AppList>("http://rventure2019.net:8282/DealzApi/api/SteamData");
    }

    getGameInfo(steamAppId: number){
        return this.httpClient.get<GameRoot>("http://rventure2019.net:8282/DealzApi/api/SteamData/" + steamAppId);
    }
}